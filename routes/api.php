<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api', 'cors'], 'namespace' => 'Apis'], function() {
    // Employee Route & Login
    Route::post('/action/login', 'EmployeeController@Login');
    Route::get('/employee', 'EmployeeController@allEmployee');
    Route::post('/action/check', 'checkerController@checkToken');
    Route::get('/employee/{nik}', 'EmployeeController@getSingleEmployee');
    Route::get('/employee/{nik}/history', 'EmployeeController@getHistory');
    // Booking Route
    Route::post('/booking/add', 'BookingController@addBooking');
    Route::get('/booking/check/tanggal/{tanggal}/user/{nik}', 'BookingController@checkBookingAndNIK');
    Route::get('/booking/check/tanggal/{tanggal}', 'BookingController@checkBooking');
    Route::get('/booking/check/jam/{jam}/tanggal/{tanggal}/user/{nik}', 'BookingController@getSingleBooking');
    Route::post('/booking/cancel/{id}', 'BookingController@cancelBookingTiket');
    Route::post('/booking/generate/passcode', 'BookingController@generatePasscode');
    // Tiket Route
    Route::get('/tiket/show/{id}', 'BookingController@getSingleTiket');
    // Information Route
    Route::get('/information/highlight', 'BookingController@checkJadwal');
    // Activity Route
    Route::post('/activity/{nik}', 'ActivityController@appActivity');
    Route::get('/ip', 'ActivityController@getIP');
    Route::get('/activity/checkin/{card}', 'ActivityController@checkIn');
    Route::get('/activity/employee/card', 'ActivityController@gettingToken');
    Route::get('/activity/checkin/passcode', 'ActivityController@passcodeCheckin');

    // Logging Route
    Route::get('/log/check/passcode', 'LoggingController@checkPasscode');
    Route::get('/log/list/today', 'LoggingController@listCheckin');
    Route::get('/log/checkin', 'LoggingController@scanCode');
    Route::get('/log/last', 'LoggingController@checkStatusLastCheckin');
    Route::get('/log/expired', 'LoggingController@expiredTimeChangeStatus');

    // Siaga Route
    Route::post('/siaga/create', 'SiagaController@createSchedule');
    Route::get('/siaga/schedule', 'SiagaController@schedule');
    Route::post('/siaga/edit', 'SiagaController@editSchedule');
    Route::get('/siaga/remove', 'SiagaController@removeSchedule');
    Route::get('/siaga/mobile/schedule', 'SiagaController@mSchedule');
    Route::get('/siaga', 'SiagaController@changeStatusSiaga');
    Route::get('/siaga/status', 'SiagaController@statusSiaga');

    // LMTS Route
    Route::get('/lmts/all', 'LMTController@getActivePost');
    Route::get('/lmts/video/{id}', 'LMTController@showVideo');
});

Route::get('/lamp', 'Apis\ActivityController@turnOnLamp');
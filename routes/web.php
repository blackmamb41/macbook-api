<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Portal IT Apps
$this->get('/home', 'HomeController@index');

Route::group(['namespace' => 'Dashboard', 'middleware' => 'auth'], function() {
    // Dashboard
    Route::get('/', 'HomeController@index');

    // Core
    Route::post('/login/action', 'HomeController@logging');

    // Employee
    Route::get('/employee', 'EmployeeController@tableEmployee');
    Route::get('/employee/new', 'EmployeeController@new');
    Route::post('employee/new/create', 'EmployeeController@create');
    Route::get('/employee/edit/{nik}', 'EmployeeController@edit');
    Route::post('/employee/edit/{nik}/store', 'EmployeeController@store');
    Route::get('/employee/status/{nik}', 'EmployeeController@status');
    Route::get('/employee/destroy/{nik}', 'EmployeeController@destroy');

    // Bookings
    Route::get('/booking', 'BookingController@index');
    Route::get('/booking/destroy/{id}', 'BookingController@destroy');
    Route::get('/booking/edit/{id}', 'BookingController@edit');
    Route::get('/booking/checkin/{card}', 'BookingController@checkIn');

    // Activities
    Route::get('/activities', 'ActivityController@index');

    // Siaga
    Route::get('/siaga', 'SiagaController@home');

    // LMTS
    Route::get('/lmts', 'LMTController@index');
    Route::get('/lmts/add', 'LMTController@add');
    Route::post('/lmts/create', 'LMTController@create');
    Route::get('/lmts/edit/{id}', 'LMTController@edit');
    Route::get('/lmts/delete/{id}', 'LMTController@delete');
});

// AUTH Controller Provide Laravel
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');
$this->get('unauthorized', 'Auth\LoginController@unauthorized');
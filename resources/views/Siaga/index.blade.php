@extends('../Core/index')

@section('title')
    Employee
@endsection

@section('styles')
	<link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="content-header row">
	</div>
	<div class="content-body">
		<section id="siaga">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Siaga Schedule</h4>
                            <br />
                            <input type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-onstyle="success" data-offstyle="danger" id="status">
						</div>
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-iconfall">
                                <li class="nav-item">
                                    <a class="nav-link active" id="baseIcon-tab41" data-toggle="tab" aria-controls="tabS1" href="#tabS1" aria-expanded="false"><i class="fa fa-arrow-down"></i>S1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="baseIcon-tab42" data-toggle="tab" aria-controls="tabS2" href="#tabS2" aria-expanded="false"><i class="fa fa-arrow-down"></i>S2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="baseIcon-tab43" data-toggle="tab" aria-controls="tabS3" href="#tabS3" aria-expanded="false"><i class="fa fa-arrow-down"></i>S3</a>
                                </li>
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane active" id="tabS1" aria-expanded="true" aria-labelledby="baseIcon-tab41">
                                    <div class="row" id="rowTabS1">
                                        <div class="col-lg-4 align-items-center row justify-content-md-center">
                                            <i class="ft-plus-circle" style="font-size: 80px; cursor: pointer;" id="addScheduleS1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tabS2" aria-labelledby="baseIcon-tab42">
                                    <div class="row" id="rowTabS2">
                                        <div class="col-lg-4 align-items-center row justify-content-md-center">
                                            <i class="ft-plus-circle" style="font-size: 80px; cursor: pointer;" id="addScheduleS2"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tabS3" aria-labelledby="baseIcon-tab43">
                                    <div class="row" id="rowTabS3">
                                        <div class="col-lg-4 align-items-center row justify-content-md-center">
                                            <i class="ft-plus-circle" style="font-size: 80px; cursor: pointer;" id="addScheduleS3"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
            <div class="modal fade text-center" id="saving" tabindex="-1" role="dialog" aria-labelledby="saving" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="color: #fff; background-color: #39DAA9; border-bottom: none;">
                            <h4 class="modal-title" id="saving" style="margin: 0 auto;">Saving</h4>
                        </div>
                        <div class="modal-body" style="background-color: #39DAA9; color: #fff">
                            <i class="fa fa-spinner spinner" style="font-size: 90px; color: "></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade text-center" id="removing" tabindex="-1" role="dialog" aria-labelledby="removing" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="color: #fff; background-color: #FF7588; border-bottom: none;">
                            <h4 class="modal-title" id="removing" style="margin: 0 auto;">Removing</h4>
                        </div>
                        <div class="modal-body" style="background-color: #FF7588; color: #fff">
                            <i class="fa fa-spinner spinner" style="font-size: 90px; color: "></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade text-center" id="notfill" tabindex="-1" role="dialog" aria-labelledby="removing" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="removing" style="margin: 0 auto;">Error</h4>
                        </div>
                        <div class="modal-body">
                            Data Belum Lengkap
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade text-center" id="exist" tabindex="-1" role="dialog" aria-labelledby="removing" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="removing" style="margin: 0 auto;">Error</h4>
                        </div>
                        <div class="modal-body">
                            Data Sudah Ada
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade text-center" id="error" tabindex="-1" role="dialog" aria-labelledby="removing" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="removing" style="margin: 0 auto;">Error</h4>
                        </div>
                        <div class="modal-body">
                            Ops.. Something Wrong
                        </div>
                    </div>
                </div>
            </div>
		</section>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/jszip.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.print.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <!-- <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script> -->
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/navs/navs.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"></script>
    <script type="text/javascript">

        // List Shift 1
        $.ajax({
            type: "GET",
            url: '/api/siaga/schedule?s=1',
            dataType: 'json',
        }).done(function(res) {
            $.each(res, function(idx, obj) {
                var iddate = $(".form-control[id^='datetimepickerSatu'").length+1;
                var idlistuser = $(".select2[id^='listUserSatu'").length+1;
                var buttonRemove = $(".buttonremove[id^='removeScheduleSatu'").length+1;
                var optionUser = '';
                $.each(obj.selected, function(idx, obj) {
                    optionUser += '<option value="'+obj.id+'" selected>'+obj.name+'</option>'
                })
                $.each(obj.notlisted, function(idx, obj) {
                    optionUser += '<option value="'+obj.id+'">'+obj.name+'</option>'
                })
                $('#rowTabS1 .col-lg-4:last').before('<div class="col-lg-4 kotakjadwal" id="'+obj.id+'"><div class="card" style="border: 1px solid #888888; border-radius: 5px"><div class="card-content" style="padding-left: 5%; padding-right: 5%"><div class="card-body"><div class="form-group"><label for="">Date :</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><span class="fa fa-calendar-o"></span></span></div><input type="text" class="form-control" id="datetimepickerSatu'+iddate+'" value="'+obj.tanggal+'"/></div></div><hr /><div style="margin-bottom: 5%;"><label>User List :</label><select class="select2 form-control" multiple="multiple" id="listUserSatu'+idlistuser+'" name="listUserSatu'+idlistuser+'">'+optionUser+'</select></div><div style="margin-bottom: 5%;"><div class="forID" id=""><button type="button" class="btn btn-danger btn-block buttonremove" id="removeScheduleSatu" name="'+obj.id+'">Remove</button></div></div></div></div></div></div>')
                $('#datetimepickerSatu'+iddate).datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#listUserSatu'+idlistuser).select2();
            })
        })

        // List Shift 2
        $.ajax({
            type: "GET",
            url: '/api/siaga/schedule?s=2',
            dataType: 'json',
        }).done(function(res) {
            $.each(res, function(idx, obj) {
                var iddate = $(".form-control[id^='datetimepickerDua'").length+1;
                var idlistuser = $(".select2[id^='listUserDua'").length+1;
                var buttonRemove = $(".buttonremove[id^='removeScheduleDua'").length+1;
                var optionUser = '';
                $.each(obj.selected, function(idx, obj) {
                    optionUser += '<option value="'+obj.id+'" selected>'+obj.name+'</option>'
                })
                $.each(obj.notlisted, function(idx, obj) {
                    optionUser += '<option value="'+obj.id+'">'+obj.name+'</option>'
                })
                $('#rowTabS2 .col-lg-4:last').before('<div class="col-lg-4 kotakjadwal" id="'+obj.id+'"><div class="card" style="border: 1px solid #888888; border-radius: 5px"><div class="card-content" style="padding-left: 5%; padding-right: 5%"><div class="card-body"><div class="form-group"><label for="">Date :</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><span class="fa fa-calendar-o"></span></span></div><input type="text" class="form-control" id="datetimepickerDua'+iddate+'" value="'+obj.tanggal+'"/></div></div><hr /><div style="margin-bottom: 5%;"><label>User List :</label><select class="select2 form-control" multiple="multiple" id="listUserDua'+idlistuser+'" name="listUserDua'+idlistuser+'">'+optionUser+'</select></div><div style="margin-bottom: 5%;"><div class="forID" id=""><button type="button" class="btn btn-danger btn-block buttonremove" id="removeScheduleDua" name="'+obj.id+'">Remove</button></div></div></div></div></div></div>')
                $('#datetimepickerDua'+iddate).datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#listUserDua'+idlistuser).select2();
            })
        })

        // List Shift 3
        $.ajax({
            type: "GET",
            url: '/api/siaga/schedule?s=3',
            dataType: 'json',
        }).done(function(res) {
            $.each(res, function(idx, obj) {
                var iddate = $(".form-control[id^='datetimepickerTiga'").length+1;
                var idlistuser = $(".select2[id^='listUserTiga'").length+1;
                var buttonRemove = $(".buttonremove[id^='removeScheduleTiga'").length+1;
                var optionUser = '';
                $.each(obj.selected, function(idx, obj) {
                    optionUser += '<option value="'+obj.id+'" selected>'+obj.name+'</option>'
                })
                $.each(obj.notlisted, function(idx, obj) {
                    optionUser += '<option value="'+obj.id+'">'+obj.name+'</option>'
                })
                $('#rowTabS3 .col-lg-4:last').before('<div class="col-lg-4 kotakjadwal" id="'+obj.id+'"><div class="card" style="border: 1px solid #888888; border-radius: 5px"><div class="card-content" style="padding-left: 5%; padding-right: 5%"><div class="card-body"><div class="form-group"><label for="">Date :</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><span class="fa fa-calendar-o"></span></span></div><input type="text" class="form-control" id="datetimepickerTiga'+iddate+'" /></div></div><hr /><div style="margin-bottom: 5%;"><label>User List :</label><select class="select2 form-control" multiple="multiple" id="listUserTiga'+idlistuser+'" name="listUserTiga'+idlistuser+'">'+optionUser+'</select></div><div style="margin-bottom: 5%;"><div class="forID" id=""><button type="button" class="btn btn-success btn-block buttonedit" id="editScheduleTiga" name="'+obj.id+'" data-toggle="modal" data-backdrop="false" data-target="#saving">Save Change</button><button type="button" class="btn btn-danger btn-block buttonremove" id="removeScheduleTiga" name="'+obj.id+'">Remove</button></div></div></div></div></div></div>')
                $('#datetimepickerTiga'+iddate).datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#listUserTiga'+idlistuser).select2();
            })
        })

        var users='';
        $.ajax({
            type: "GET",
            url: '/api/employee',
            dataType: 'json',
            contentType: "application/json",
            success: function(response) {
                users = response.data;
            }
        })
    	$(function() {
            $('#toggle-two').bootstrapToggle({
                on: 'Enabled',
                off: 'Disabled'
            });
        })
        
        // Add Form Scheuld Button Function
        $('#addScheduleS1').click(function() {
            var iddate = $(".form-control[id^='datetimepickerSatu'").length+1;
            var idlistuser = $(".select2[id^='listUserSatu'").length+1;
            var buttonSave = $(".buttonsave[id^='saveScheduleSatu'").length+1;
            var buttonRemove = $(".buttonremove[id^='removeScheduleSatu'").length+1;
            var optionUser = '';
            var divID = '';
            $.each(users, function(idx, obj) {
                optionUser += '<option value="'+obj.id+'">'+obj.name+'</option>'
            })
            $('#rowTabS1 .col-lg-4:last').before('<div class="col-lg-4 kotakjadwal" id="'+iddate+'"><div class="card" style="border: 1px solid #888888; border-radius: 5px"><div class="card-content" style="padding-left: 5%; padding-right: 5%"><div class="card-body"><div class="form-group"><label for="">Date :</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><span class="fa fa-calendar-o"></span></span></div><input type="text" class="form-control" id="datetimepickerSatu'+iddate+'" /></div></div><hr /><div style="margin-bottom: 5%;"><label>User List :</label><select class="select2 form-control" multiple="multiple" id="listUserSatu'+idlistuser+'" name="listUserSatu'+idlistuser+'">'+optionUser+'</select></div><div style="margin-bottom: 5%;"><div class="forID" id=""><button type="button" class="btn btn-success btn-block buttonsave" id="saveScheduleSatu" name="'+iddate+'">Save Change</button><button type="button" class="btn btn-danger btn-block buttonremove" id="removeScheduleSatu'+buttonRemove+'" data-toggle="modal" data-backdrop="false" data-target="#removing" data-keyboard="false" data-toggle="modal">Remove</button></div></div></div></div></div></div>')
            $('#datetimepickerSatu'+iddate).datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#listUserSatu'+idlistuser).select2();
        })
        $('#addScheduleS2').click(function() {
            var iddate = $(".form-control[id^='datetimepickerDua'").length+1;
            var idlistuser = $(".select2[id^='listUserDua'").length+1;
            var buttonSave = $(".buttonsave[id^='saveScheduleDua'").length+1;
            var buttonRemove = $(".buttonremove[id^='removeScheduleDua'").length+1;
            var optionUser = '';
            var divID = '';
            $.each(users, function(idx, obj) {
                optionUser += '<option value="'+obj.id+'">'+obj.name+'</option>'
            })
            $('#rowTabS2 .col-lg-4:last').before('<div class="col-lg-4 kotakjadwal" id="'+iddate+'"><div class="card" style="border: 1px solid #888888; border-radius: 5px"><div class="card-content" style="padding-left: 5%; padding-right: 5%"><div class="card-body"><div class="form-group"><label for="">Date :</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><span class="fa fa-calendar-o"></span></span></div><input type="text" class="form-control" id="datetimepickerDua'+iddate+'" /></div></div><hr /><div style="margin-bottom: 5%;"><label>User List :</label><select class="select2 form-control" multiple="multiple" id="listUserDua'+idlistuser+'" name="listUserDua'+idlistuser+'">'+optionUser+'</select></div><div style="margin-bottom: 5%;"><div class="forID" id=""><button type="button" class="btn btn-success btn-block buttonsave" id="saveScheduleDua" name="'+iddate+'" data-toggle="modal" data-backdrop="false" data-target="#saving">Save Change</button><button type="button" class="btn btn-danger btn-block buttonremove" id="removeScheduleDua'+buttonRemove+'" data-toggle="modal" data-backdrop="false" data-target="#removing" data-keyboard="false" data-toggle="modal">Remove</button></div></div></div></div></div></div>')
            $('#datetimepickerDua'+iddate).datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#listUserDua'+idlistuser).select2();
        })
        $('#addScheduleS3').click(function() {
            var iddate = $(".form-control[id^='datetimepickerTiga'").length+1;
            var idlistuser = $(".select2[id^='listUserTiga'").length+1;
            var buttonSave = $(".buttonsave[id^='saveScheduleTiga'").length+1;
            var buttonRemove = $(".buttonremove[id^='removeScheduleTiga'").length+1;
            var optionUser = '';
            var divID = '';
            $.each(users, function(idx, obj) {
                optionUser += '<option value="'+obj.id+'">'+obj.name+'</option>'
            })
            $('#rowTabS3 .col-lg-4:last').before('<div class="col-lg-4 kotakjadwal" id="'+iddate+'"><div class="card" style="border: 1px solid #888888; border-radius: 5px"><div class="card-content" style="padding-left: 5%; padding-right: 5%"><div class="card-body"><div class="form-group"><label for="">Date :</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><span class="fa fa-calendar-o"></span></span></div><input type="text" class="form-control" id="datetimepickerTiga'+iddate+'" /></div></div><hr /><div style="margin-bottom: 5%;"><label>User List :</label><select class="select2 form-control" multiple="multiple" id="listUserTiga'+idlistuser+'" name="listUserTiga'+idlistuser+'">'+optionUser+'</select></div><div style="margin-bottom: 5%;"><div class="forID" id=""><button type="button" class="btn btn-success btn-block buttonsave" id="saveScheduleTiga" name="'+iddate+'" data-toggle="modal" data-backdrop="false" data-target="#saving">Save Change</button><button type="button" class="btn btn-danger btn-block buttonremove" id="removeScheduleTiga'+buttonRemove+'" data-toggle="modal" data-backdrop="false" data-target="#removing" data-keyboard="false" data-toggle="modal">Remove</button></div></div></div></div></div></div>')
            $('#datetimepickerTiga'+iddate).datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#listUserTiga'+idlistuser).select2();
        })

        // S1 Function Add Button
        $(document).on('click', '#saveScheduleSatu', function() {
            var id = $(this).attr('name');
            var tgl = $('#datetimepickerSatu'+id).val();
            var users = $('#listUserSatu'+id).val();
            console.log(tgl);
            var shift = 1

            if(tgl == '' || users == '') {
                $('#notfill').modal('show')
            }else{
                $('#saving').modal({
                    keyboard: false,
                    backdrop: false
                })
                $('#saving').modal('show');
                $.ajax({
                    type: 'POST',
                    url: '/api/siaga/create?tanggal='+tgl+'&s='+shift,
                    dataType: 'json',
                    data: {
                        users: users
                    }
                }).done(function(res) {
                    $('#saving').modal('hide')
                    location.reload()
                }).fail(function(err) {
                    if(err.status == 409){
                        $('#saving').modal('hide')
                        $('#exist').modal('show')
                    }else{
                        $('#saving').modal('hide')
                        $('#error').modal('show')
                    }
                })
            }
        })

        // S2 Function Add Button
        $(document).on('click', '#saveScheduleDua', function() {
            var id = $(this).attr('name');
            var tgl = $('#datetimepickerDua'+id).val();
            var users = $('#listUserDua'+id).val();
            console.log(tgl);
            var shift = 2

            if(tgl == '' || users == '') {
                $('#notfill').modal('show')
            }else{
                $('#saving').modal({
                    keyboard: false,
                    backdrop: false
                })
                $('#saving').modal('show');
                $.ajax({
                    type: 'POST',
                    url: '/api/siaga/create?tanggal='+tgl+'&s='+shift,
                    dataType: 'json',
                    data: {
                        users: users
                    }
                }).done(function(res) {
                    $('#saving').modal('hide')
                    location.reload()
                }).fail(function(err) {
                    if(err.status == 409){
                        $('#saving').modal('hide')
                        $('#exist').modal('show')
                    }else{
                        $('#saving').modal('hide')
                        $('#error').modal('show')
                    }
                })
            }
        })

        // S3 Function Add Button
        $(document).on('click', '#saveScheduleTiga', function() {
            var id = $(this).attr('name');
            var tgl = $('#datetimepickerTiga'+id).val();
            var users = $('#listUserTiga'+id).val();
            console.log(tgl);
            var shift = 3

            if(tgl == '' || users == '') {
                $('#notfill').modal('show')
            }else{
                $('#saving').modal({
                    keyboard: false,
                    backdrop: false
                })
                $('#saving').modal('show');
                $.ajax({
                    type: 'POST',
                    url: '/api/siaga/create?tanggal='+tgl+'&s='+shift,
                    dataType: 'json',
                    data: {
                        users: users
                    }
                }).done(function(res) {
                    $('#saving').modal('hide')
                    location.reload()
                }).fail(function(err) {
                    if(err.status == 409){
                        $('#saving').modal('hide')
                        $('#exist').modal('show')
                    }else{
                        $('#saving').modal('hide')
                        $('#error').modal('show')
                    }
                })
            }
        })

        //S1 Remove Button
        $(document).on('click', '#removeScheduleSatu', function() {
            var id = $(this).attr('name');
            $('#removing').modal('show')
            $.ajax({
                type: 'GET',
                url: '/api/siaga/remove',
                dataType: 'json',
                data: {
                    idSchedule: id
                }
            }).done(function(res) {
                $('#removing').modal('hide')
                location.reload()
            })
        })

        //Status Button Change
        var stat;
        $("#status").change(function() {
            if($(this).prop("checked") == true){
                stat = 1
            }else{
                stat = 0
            }

            $.ajax({
                type: 'GET',
                url: '/api/siaga?status='+stat,
                dataType: 'json',
            }).done(function(res) {
                console.log(res)
            })
        })

        $.ajax({
            type: 'GET',
            url: '/api/siaga/status',
            dataType: 'json',
        }).done(function(res) {
            if(res.value == 1){
                $('#status').bootstrapToggle('on')
            }else{
                $('#status').bootstrapToggle('off')
            }
        })

        // S1 Function Edit Button
        // $(document).on('click', "#editScheduleSatu", function() {
        //     var idForm = $(this).attr('name');
        //     var schedule = $('.kotakjadwal').attr('id');
        //     var tgl = $('#datetimepickerSatu'+idForm).val();
        //     var users = $('#listUserSatu'+idForm).val();
        //     var shift = 1
        //     console.log(schedule);
        //     if(tgl == '' || users == '') {
        //         $('#notfill').modal('show')
        //     }else{
        //         $('#saving').modal({
        //             keyboard: false,
        //             backdrop: false
        //         })
        //         $('#saving').modal('show');
        //         $.ajax({
        //             type: "POST",
        //             url: '/api/siaga/edit?id='+schedule,
        //             dataType: 'json',
        //             data: {
        //                 tanggal: tgl,
        //                 shift: shift,
        //                 users: users
        //             }
        //         }).done(function(res) {
        //             $('#saving').modal('hide')
        //             // location.reload()
        //             console.log(res)
        //         }).fail(function(err) {
        //             console.log(err)
        //             $('#saving').modal('hide')
        //             $('#error').modal('show')
        //         })
        //     }
        // })

    </script>
@endsection
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('app-assets/css/vendors.css') }}">
    @yield('styles')
</head>
<body>
    <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-header">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item">
                            <a class="navbar-brand" href="/">
                                <img style="width: 25px; height: 25px;" class="brand-logo" alt="LOGO MAC" src="{{ asset('logo/telkomsel.png')}}">
                                <h2 class="brand-text">RAMOS</h2>
                            </a>
                        </li>
                        <li class="nav-item d-md-none">
                            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-container content">
                    <div class="collapse navbar-collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li> 
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <li class="dropdown dropdown-user nav-item">
                                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                    <span class="avatar avatar-online">
                                        <img src="{{ asset('app-assets/images/portrait/small/avatar-s-1.png')}}" alt="avatar"><i></i>
                                    </span>
                                    <span class="user-name">{{ Auth::user()->name }}</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">
                                        <i class="ft-user"></i> Edit Profile</a>
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="ft-log-out"></i>
                                            Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" nav-item {{ Request::is('/') ? 'active' : '' }}"><a href="/"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a></li>
                    <li class=" nav-item {{ Request::is('/activites') ? 'active' : ''}}"><a href="/activities"><i class="ft-activity"></i><span class="menu-title" data-i18n="">Activity</span></a></li>
                    <li class=" nav-item {{ request()->is('/employee') ? 'active' : ''}}"><a href="/employee"><i class="ft-users"></i><span class="menu-title" data-i18n="">Employee</span></a></li>
                    <li class=" nav-item {{ Request::is('/booking') ? 'active' : '' }}"><a href="/booking"><i class="ft-book"></i><span class="menu-title" data-i18n="">Bookings</span></a></li>
                    <li class=" nav-item {{ Request::is('/siaga') ? 'active' : '' }}"><a href="/siaga"><i class="ft-anchor"></i><span class="menu-title" data-i18n="">Siaga</span></a></li>
                    <li class=" nav-item"><a href="/lmts"><i class="ft-airplay"></i><span class="menu-title" data-i18n="">LMT</span></a></li>
                    <li class=" nav-item"><a href="/"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span></a></li>
                </ul>
            </div>
        </div>
        <div class="app-content content">
            <div class="content-wrapper">
                @yield('content')
            </div>
        </div>
        <script src="{{ asset('app-assets/vendors/js/vendors.min.js')}}"></script>
        @yield('scripts')
    </body>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IT Support Regional Jabar - Portal Apps</title>
    <link rel="stylesheet" href="{{ asset('portal/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('portal/fonts/line-icons.css')}}"> -->
    <link href="https://cdn.lineicons.com/1.0.1/LineIcons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('portal/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('portal/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{ asset('portal/css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('portal/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('portal/css/responsive.css')}}">
</head>
<body>
    <header id="header-warp">
        <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
            <div class="container">
                <a href="index.html" class="navbar-brand"><img src="{{ asset('logo/tsel.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="lni-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
                        <li class="nav-item active">
                            <a class="nav-link" href="#hero-area">
                                Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#services">
                                Apps
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#meeting">
                                Meeting Today
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">
                                Contact Us
                            </a>
                        </li>
                        <li class="nav-item">
                            @if($isLogin == false)
                                <a href="/" class="btn btn-common">Login</a>
                            @else
                                <a class="btn btn-common" href="#">
                                    {{ Auth::user()->name }}
                                </a>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="hero-area-bg" id="hero-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
                            <h2 class="head-title">
                                Welcome To IT Support Jabar Portal Page
                            </h2>
                            <p>
                                Halaman ini khusus untuk membantu user dalam kegiatan
                                seperti Meeting, dan juga mensupport teman-teman demi
                                kelangsungan pekerjaannya yang relevan dengan kita.
                            </p>
                            <br />
                            <div class="header-button">
                                <a href="{{ asset('apk/mangga.apk')}}" class="btn btn-common">Download MANGGA Apps</a>
                                <a href="/mangga/" class="btn btn-border video-popup">Booking via Web</a>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                            <div class="intro-img">
                                <img class="img-fluid" src="#" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="services" class="section-padding">
        <div class="container">
            <div class="section-header text-center">
                <h2 class="section-title wow fadeInDown" data-wow-delay="0.2s">IT Support Apps</h2>
                <div class="shape wow fadeInDown" data-wow-delay="0.1s"></div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.1s">
                        <div class="icon">
                            <i class="lni-seo-monitoring"></i>
                        </div>
                        <div class="services-content">
                            <h3><a href="http://10.3.2.89/zabbix">Zabbix Monitoring</a></h3>
                            <p>Traffic Monitoring Page. halaman ini berfungsi untuk memantau traffic 
                                yang diperangkat IT Seluruh Regional Jabar, agar tetap terjaga bekerjanya
                                perangkat-perangkat Network maupun NE lainnya.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.1s">
                        <div class="icon">
                            <i class="lni-cloud-upload"></i>
                        </div>
                        <div class="services-content">
                            <h3><a href="http://10.3.98.217/drive">IT Cloud</a></h3>
                            <p>IT Cloud Page. halaman ini berfungsi untuk mengarahkan tim IT
                                ke halaman drive yang berguna untuk membackup data-data penting.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.1s">
                        <div class="icon">
                            <i class="lni-bar-chart"></i>
                        </div>
                        <div class="services-content">
                            <h3><a href="http://10.3.98.217/itjabar">DAPOT IT</a></h3>
                            <p>IT Cloud Page. halaman ini berfungsi untuk mengarahkan tim IT
                                ke halaman drive yang berguna untuk membackup data-data penting.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xs-12">
                    <div class="services-item wow fadeInRight" data-wow-delay="0.1s">
                        <div class="icon">
                            <i class="lni-ux"></i>
                        </div>
                        <div class="services-content">
                            <h3><a href="http://10.3.98.217">MANGGA</a></h3>
                            <p>IT Cloud Page. halaman ini berfungsi untuk mengarahkan tim IT
                                ke halaman drive yang berguna untuk membackup data-data penting.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('portal/js/jquery-min.js') }}"></script>
    <script src="{{ asset('portal/js/popper.min.js')}}"></script>
    <script src="{{ asset('portal/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('portal/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('portal/js/wow.js')}}"></script>
    <script src="{{ asset('portal/js/jquery.nav.js')}}"></script>
    <script src="{{ asset('portal/js/scrolling-nav.js')}}"></script>
    <script src="{{ asset('portal/js/jquery.easing.min.js')}}"></script>  
    <script src="{{ asset('portal/js/main.js')}}"></script>
    <script src="{{ asset('portal/js/form-validator.min.js')}}"></script>
    <script src="{{ asset('portal/js/contact-form-script.min.js')}}"></script>
</body>
</html>
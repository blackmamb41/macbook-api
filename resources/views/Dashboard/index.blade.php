@extends('../Core/index')

@section('title')
    Dashboard
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('app-assets/vendors/css/extensions/unslider.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/vendors/css/charts/morris.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/vendors/css/extensions/unslider.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/vendors/css/weather-icons/climacons.min.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/fonts/meteocons/style.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/css/pages/timeline.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/css/core/colors/palette-gradient.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/fonts/simple-line-icons/style.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
<link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
@endsection

@section('content')
    <div class="content-header row"></div>
    <div class="content-body">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="media align-items-stretch">
                            <div class="p-2 text-center bg-primary bg-darken-2">
                                <i class="icon-users font-large-2 white"></i>
                            </div>
                            <div class="p-2 bg-gradient-x-primary white media-body">
                                <h5>Employee</h5>
                                <h5 class="text-bold-400 mb-0">{{ $employee }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="media align-items-stretch">
                            <div class="p-2 text-center bg-success bg-darken-2">
                                <i class="icon-book-open font-large-2 white"></i>
                            </div>
                            <div class="p-2 bg-gradient-x-success white media-body">
                                <h5>Tikets</h5>
                                <h5 class="text-bold-400 mb-0">{{ $tiket }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="media align-items-stretch">
                            <div class="p-2 text-center bg-warning bg-darken-2">
                                <i class="icon-info font-large-2 white"></i>
                            </div>
                            <div class="p-2 bg-gradient-x-warning white media-body">
                                <h5>Pending</h5>
                                <h5 class="text-bold-400 mb-0">{{ $pending }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="media align-items-stretch">
                            <div class="p-2 text-center bg-danger bg-darken-2">
                                <i class="icon-close font-large-2 white"></i>
                            </div>
                            <div class="p-2 bg-gradient-x-danger white media-body">
                                <h5>Cancel</h5>
                                <h5 class="text-bold-400 mb-0">{{ $cancel }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row match-height">
            <div class="col-xl-8 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Products Sales</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div id="products-sales" class="height-300"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Today Booked</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content px-1">
                        <div id="recent-buyers" class="media-list height-300 position-relative">
                            @foreach($today as $booked)
                                @if(empty($booked))
                                <p>ANYONE NOT BOOKED YET TODAY</p>
                                @else
                                <a href="#" class="media border-0">
                                    <div class="media-left pr-1">
                                        <span class="avatar avatar-md avatar-onine">
                                            <img class="media-object rounded-circle" src="{{ asset('app-assets/images/portrait/small/avatar-s-7.png')}}" alt="Generic placeholder image"><i></i>
                                        </span>
                                    </div>
                                    <div class="media-body w-100">
                                        <h6 class="list-group-item-heading">{{ $booked->name }}</h6>
                                    </div>
                                    <p class="list-group-item-text mb-0">
                                        @if($booked->status == 'pending')
                                        <span class="badge badge-warning">Pending</span>
                                        @elseif($booked->status == 'checkin')
                                        <span class="badge badge-success">Checkin</span>
                                        @endif
                                    </p>
                                </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/extensions/jquery.knob.min.js')}}"></script>
<script src="{{ asset('app-assets/js/scripts/extensions/knob.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/charts/raphael-min.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/charts/morris.min.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{ asset('app-assets/data/jvector/visitor-data.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/charts/chart.min.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/unslider-min.js')}}"></script>
<script src="{{ asset('app-assets/css/core/colors/palette-climacon.css')}}"></script>
<script src="{{ asset('app-assets/fonts/simple-line-icons/style.min.css')}}"></script>
<script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
<script src="{{ asset('app-assets/js/core/app.js')}}"></script>
<script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
<script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
<script src="{{ asset('app-assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
@endsection
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="10;url={{ route('login')}}">
    <title>LOGIN</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('app-assets/css/vendors.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/pages/under-maintenance.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
</head>
<body class="vertical-layout vertical-menu 1-column bg-full-screen-image bg-maintenance-image menu-expanded blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3 m-0">
                                <div class="card-body text-center">
                                    <span class="card-title text-center">
                                        <img src="{{ asset('logo/telkomsel.png')}}" alt="Logo" style="width: 40px; height: 40px;">
                                    </span>
                                </div>
                                <div class="card-body text-center">
                                    <h3>You are not <p style="font-weight: bold">UNAUTHORIZED</p></h3>
                                    <p>To enter this page.
                                        <br /> Because your not allowed to access this page
                                    </p>
                                    <p>You will redirecting to login page....</p><h3 id="redirect">10</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/jquery.animateNumber.min.js')}}"></script>
    <script>
        $('#redirect')
        .prop('number', 10)
        .animateNumber(
            {
                number: 0,
            },
            10000,
            'linear'
        );
    </script>
</body>
</html>
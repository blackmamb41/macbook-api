@extends('../Core/index')

@section('title')
    Tiket - {{ $data->tiket }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
@endsection

@section('content')
<div class="content-header row"></div>
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Tiket</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="card-text">
                                <p>Untuk Mengisi Form New Employee Sesuaikan dengan Data Potensi yang terupdate dan Valid</p>
                            </div>
                            <form action="/booking/save/{{$data->id}}" method="POST" class="form form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-book"></i> Tiket Info</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="tiket">Nomor Tiket</label>
                                        <div class="col-md-9">
                                            <input type="text" disabled id="tiket" class="form-control" name="tiket" value="{{ $data->tiket }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="PIC">PIC</label>
                                        <div class="col-md-9">
                                            <input type="text" disabled id="pic" class="form-control" name="pic" value="{{ $data->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="">Tanggal</label>
                                        <div class="col-md-9">
                                            <input type="text" disabled id="pic" class="form-control" name="pic" value="{{ $data->tanggal }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="">Jam</label>
                                        <div class="col-md-9">
                                            <input type="text" disabled id="pic" class="form-control" name="pic" value="{{ implode(',', unserialize($data->jam)) }}">
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="ft-sliders"></i> Automation </h4>
                                    @if($data->status == 'checkin')
                                        <div></div>
                                    @else
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="nik">Generate Passcode</label>
                                            <div class="col-md-9" id="generatingpasscode">
                                                @if(empty($data->passcode))
                                                    <button type="button" class="btn btn-success" id="passcode">Generate</button>
                                                @else
                                                    <input type="text" disabled id="generated" class="form-control" name="generated" value="{{ $data->passcode }}">
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="nik">Status</label>
                                        <div class="col-md-9">
                                            @if($data->status == 'pending')
                                                <span class="badge badge-warning">PENDING</span>
                                            @elseif($data->status == 'checkin')
                                                <span class="badge badge-success">CHECKIN</span>
                                            @elseif($data->status == 'cancel')
                                                <span class="badge badge-danger">CANCEL</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="/booking">
                                            <button type="button" class="btn btn-danger mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                        </a>
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script>
        var tiket = $("#tiket").val();
            $("#passcode").click(function() {
                $.post(
                "/api/booking/generate/passcode",
                {
                    nomor: tiket
                }
            ).done(function(response) {
                $("#passcode").remove();
                $("#generatingpasscode").append('<input type="text" disabled id="generated" class="form-control" name="generated" value="'+response.passcode+'">')
                console.log(response);
            })
        })
    </script>
@endsection
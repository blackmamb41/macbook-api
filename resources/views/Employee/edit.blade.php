@extends('../Core/index')

@section('title')
    Edit - {{ $data->name }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('ssets/css/style.css')}}">
@endsection

@section('content')
<div class="content-header row"></div>
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">New Employee</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="card-text">
                                <p>Untuk Mengisi Form New Employee Sesuaikan dengan Data Potensi yang terupdate dan Valid</p>
                            </div>
                            <form action="/employee/edit/{{$data->nik}}/store" method="POST" class="form form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="name" class="form-control" placeholder="Nama Lengkap" name="name" value="{{ $data->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="card">Card ID</label>
                                        <div class="col-md-9">
                                            <input type="text" id="nik" class="form-control" placeholder="Card ID" name="card_id" value="{{ $data->card_id }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Password</label>
                                        <div class="col-md-9" id="changepassword">
                                            <!-- <input type="text" id="password" class="form-control" placeholder="Password" name="password"> -->
                                            <button type="button" class="btn btn-primary" id="passwordbtn">Change Password</button>
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="ft-clipboard"></i> Requirements</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="nik">NIK</label>
                                        <div class="col-md-9">
                                            <input type="text" id="nik" class="form-control" placeholder="Nomor Induk Karyawan" name="nik" value="{{ $data->nik }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="email">E-Mail</label>
                                        <div class="col-md-9">
                                            <input type="text" id="email" class="form-control" placeholder="Alamat E-Mail" name="email" value="{{ $data->email }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Division</label>
                                        <div class="col-md-9">
                                            <input type="text" id="division" class="form-control" placeholder="Divisi" name="division" value="{{ $data->division }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Position</label>
                                        <div class="col-md-9">
                                            <input type="text" id="position" class="form-control" placeholder="Posisi" name="position" value="{{ $data->position }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Telephone</label>
                                        <div class="col-md-9">
                                            <input type="text" id="telp" class="form-control" placeholder="Nomor Telephone" name="telp" value="{{ $data->telp }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Photo</label>
                                        <div class="col-md-9">
                                            <label id="photo" class="file center-block">
                                                <input type="file" id="photo" name="photo">
                                                <span class="file-custom"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="name">Administrator</label>
                                        <div class="col-md-4">
                                            <select name="isAdmin" id="isAdmin" class="form-control">
                                                @if($data->isAdmin == 1)
                                                    <option value="0">Member</option>
                                                    <option value="1" selected>Administrator</option>
                                                @else
                                                    <option value="0" selected>Member</option>
                                                    <option value="1">Administrator</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="/employee">
                                            <button type="button" class="btn btn-danger mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                        </a>
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script>
        $("#passwordbtn").click(function() {
            $("#passwordbtn").remove();
            $("#changepassword").append('<input type="password" id="password" class="form-control" placeholder="New Password" name="password" style="width: 50%;~">');
        });
    </script>
@endsection
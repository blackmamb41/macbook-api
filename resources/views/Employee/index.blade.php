@extends('../Core/index')

@section('title')
    Employee
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="employee-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Employee DAPOT</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                            <a href="/employee/new">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-plus"></i> Create New Employee</button>
                            </a>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered file-export" id="employees">
                                        <thead style="background-color: rgba(46, 49, 49, 1); color:white;">
                                            <tr>
                                                <th style="text-align:center">NIK</th>
                                                <th style="text-align:center">Card Id</th>
                                                <th style="text-align:center">Name</th>
                                                <th style="text-align:center">E-Mail</th>
                                                <th style="text-align:center">Status</th>
                                                <th style="text-align:center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $item)
                                            <tr>
                                                <td>{{ $item->nik }}</td>
                                                <td>{{ $item->card_id }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->email }}</td>
                                                @if($item->status == 1)
                                                    <td><span class="badge badge-success">Active</span></td>
                                                @else
                                                    <td><span class="badge badge-danger">Inactive</span></td>
                                                @endif
                                                <td>
                                                    <a href="/employee/edit/{{ $item->nik }}"><i class="ft-edit-2" style="color:blue"></i></a>&nbsp;&nbsp;
                                                    <a href="/employee/status/{{ $item->nik }}">
                                                        @if($item->status == 0)
                                                            <i class="ft-x-circle" style="color:red"></i>
                                                        @else
                                                            <i class="ft-check-circle" style="color:green"></i>
                                                        @endif
                                                    </a>&nbsp;&nbsp;
                                                    <a href="/employee/destroy/{{ $item->nik }}"><i class="ft-trash" style="color:black"></i></a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/jszip.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"></script>
    <script>
        $("#employees").DataTable({
            ordering: false,
            order: [ 2, 'asc' ]
        })
    </script>
@endsection
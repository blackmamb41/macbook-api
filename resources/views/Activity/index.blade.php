@extends('../Core/index')

@section('title')
    Activity
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="employee-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Activity List</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered file-export" id="employees">
                                        <thead style="background-color: rgba(46, 49, 49, 1); color:white;">
                                            <tr>
                                                <th style="text-align:center">PIC</th>
                                                <th style="text-align:center">IP</th>
                                                <th style="text-align:center">Tanggal</th>
                                                <th style="text-align:center">Jam</th>
                                                <th style="text-align:center">Activity</th>
                                                <th style="text-align:center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $item)
                                            <tr>
                                                <td style="text-align:center">{{ $item->name }}</td>
                                                <td style="text-align:center">{{ $item->ip }}</td>
                                                <td style="text-align:center">{{ $item->tanggal }}</td>
                                                <td style="text-align:center">{{ $item->jam }}</td>
                                                <td>{{ $item->activity }}</td>
                                                <td style="text-align:center">
                                                    <a href="/acitivity/show/{{ $item->id }}"><i class="ft-edit-2" style="color:blue"></i></a>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/jszip.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"></script>
    <script>
        $("#employees").DataTable({
            ordering: false,
            order: [ 2, 'asc' ]
        })
    </script>
@endsection
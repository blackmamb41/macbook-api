@extends('../Core/index')

@section('title', 'Create New Article')

@section('styles')
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('content')
    <div class="content-header row"></div>
    <div class="content-body">
        <section class="basic-elements">
            <form action="/lmts/create" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-8" style="float: none; margin: 0 auto;">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="text-align: center">New Article</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="title" style="font-weight: bold; text-decoration: underline">TITLE :</label>
                                                <input type="text" class="form-control" id="title" placeholder="Put Title Here...." name="title" required>
                                            </fieldset>
                                        </div>
                                        <div class="col-xl-12 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="description" style="font-weight: bold">Description :</label>
                                                <textarea class="form-control" name="description" id="description" rows="15" placeholder="Optional Description ....."></textarea>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" style="float: none; margin: 0 auto;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-6 col-md-12 mb-1">
                                                    <fieldset class="form-group" style="text-align: center">
                                                        <label for="video" style="font-weight: bold;">VIDEO PODCAST :</label>
                                                        <div class="file-drop-area">
                                                            <span class="fake-btn"><i class="fa fa-upload"></i></span>
                                                            <span class="file-msg">or drag and drop files here</span>
                                                            <input type="file" class="file-input" name="video" id="video" accept="video/*" required>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-6 col-md-12 mb-1">
                                                    <fieldset class="form-group" style="text-align: center">
                                                        <label for="video" style="font-weight: bold;">THUMBNAIL :</label>
                                                        <div class="file-drop-area">
                                                            <span class="fake-btn"><i class="fa fa-upload"></i></span>
                                                            <span class="file-msg">or drag and drop files here</span>
                                                            <input type="file" class="file-input" id="thumbnail" name="thumbnail" accept="image/*" required />
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="float: none; margin: 0 auto">
                        <div class="col-xl-12 col-lg-6 col-md-12 mb-1">
                            <button type="submit" class="mr-1 mb-1 btn btn-success btn-min-width" style="width: 100%"><i class="fa fa-check"></i> CREATE</button>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    
@endsection

@section('scripts')
    <script src="https://cdn.tiny.cloud/1/uufqa15nfd6gk9cu7ucku9xlqwi0tg5m9yu9oreh9jx1ywvl/tinymce/5/tinymce.min.js"></script>
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/tags/form-field.js')}}"></script>
    <!-- <script src='https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js' referrerpolicy="origin"></script> -->
    <script>
        tinymce.init({
            selector: '#description'
        })

        var $fileInput = $('.file-input');
        var $droparea = $('.file-drop-area');

        // highlight drag area
        $fileInput.on('dragenter focus click', function() {
        $droparea.addClass('is-active');
        });

        // back to normal state
        $fileInput.on('dragleave blur drop', function() {
        $droparea.removeClass('is-active');
        });

        // change inner text
        $fileInput.on('change', function() {
        var filesCount = $(this)[0].files.length;
        var $textContainer = $(this).prev();

        if (filesCount === 1) {
            // if single file is selected, show file name
            var fileName = $(this).val().split('\\').pop();
                $textContainer.text(fileName);
        } else {
                // otherwise show number of files
                $textContainer.text(filesCount + ' files selected');
        }
        });
    </script>
@endsection
@extends('../Core/index')

@section('title', 'Layanan Masyarakat Telkomsel')

@section('styles')
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('content')
    <div class="content-header">
    </div>
    <div class="content-body">
        <section id="lmtList">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">LMTS</h4>
                            <a href="/lmts/add">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-plus"></i> Create New Article</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard" style="margin-left: 2%; margin-right: 2%">
                                <table class="table table-striped table-bordered" id="lmtstable">
                                    <thead style="background-color: rgba(46, 49, 49, 1); color:white;">
                                        <tr>
                                            <th style="text-align:center">Title</th>
                                            <th style="text-align:center">Status</th>
                                            <th style="text-align:center">Date</th>
                                            <th style="text-align:center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{$item->title}}</td>
                                            @if($item->status == 1)
                                                <td style="text-align:center"><span class="badge badge-success">Published</span></td>
                                            @else
                                                <td style="text-align:center"><span class="badge badge-danger">Draft</span></td>
                                            @endif
                                            <td style="text-align:center">{{$item->created_at}}</td>
                                            <td style="text-align:center">
                                                <a href="/lmts/edit/{{$item->id}}"><i class="ft-edit-2" style="color: blue"></i></a>
                                                <a href="/lmts/delete/{{$item->id}}"><i class="ft-trash" style="color: red"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/jszip.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"></script>
    <script>
        $("#lmtstable").DataTable({
            "columnDefs": [
                { "width": "55%", "targets": 0 },
                { "orderable": false, "searchable": false, "targets": [3]}
            ]
        })
    </script>
@endsection

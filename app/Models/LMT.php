<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LMT extends Model
{
    protected $table = 'lmts';

    protected $guarded = [];
}

<?php

namespace App\Http\Controllers\Auth;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'nik';
    }

    public function showLoginForm()
    {
        return view('Core/login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    public function unauthorized(Request $request)
    {
        return view('layouts/unauthorized');
    }

    protected function attemptLogin(Request $request)
    {
        $find = Employee::where('nik', $request->only($this->username()))->first();
        if($find->isAdmin == '1' && $find->status == '1')
        {
            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        } elseif($find->isAdmin == '1' && $find->status == '0') {
            return redirect('/unauthorized');
        } elseif($find->isAdmin == '0' || $find->status == '0') {
            return redirect('/unauthorized');
        } elseif($find->status == '0' && $find->isAdmin == '0') {
            return redirect('/unauthorized');
        } elseif($find->status == '0' && $find->isAdmin == '1') {
            return redirect('/unauthorized');
        }
    }

    protected function sendLoginResponse(Request $request) {
        return redirect('/unauthorized');
    }
}

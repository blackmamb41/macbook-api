<?php

namespace App\Http\Controllers\Dashboard;

use Hash;
use App\Models\LMT;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LMTController extends Controller
{
    public function index() {
        $getList = LMT::where('isDeleted', 0)->get();

        return view('LMTS/index', ['data' => $getList]);
    }

    public function add(){
        return view('LMTS/create');
    }

    public function create(Request $request) {
        $title = $request->title;
        $body = $request->description;
        $getVideo = $request->file('video');
        $video = md5($getVideo->getClientOriginalName()) . '.' . $getVideo->getClientOriginalExtension();
        $getThumbnail = $request->file('thumbnail');
        $thumbnail = md5($getThumbnail->getClientOriginalName()) . '.' . $getThumbnail->getClientOriginalExtension();
        $destFolder = 'uploads';
        
        $data = LMT::create([
            'title' => $title,
            'body'  => $body,
            'thumbnail' => $thumbnail,
            'video' => $video,
            'status' => 1,
            'isDeleted' => 0
        ]);

        $getVideo->move('uploads/videos', $video);
        $getThumbnail->move('uploads/thumbs', $thumbnail);

        return redirect('/lmts');
    }

    public function edit($id) {
        $getData = LMT::where('id', $id)->first();

        return view('LMTS/edit', ['data' => $getData]);
    }

    public function delete($id) {
        LMT::destroy($id);

        return redirect('/lmts');
    }
}

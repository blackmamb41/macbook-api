<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    public function index()
    {
        $data = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.tiket as tiket',
                    'bookings.perihal as perihal',
                    'bookings.tanggal as tanggal',
                    'bookings.jam as jam',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'bookings.created_at as created_at',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->orderBy('tanggal', 'desc')
                ->get();

        return view('Booking/index', ['data' => $data]);
    }

    public function destroy($id)
    {
        Booking::destroy($id);

        return redirect('/booking');
    }

    public function edit($id)
    {
        $data = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.tiket as tiket',
                    'bookings.perihal as perihal',
                    'bookings.tanggal as tanggal',
                    'bookings.jam as jam',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'bookings.created_at as created_at',
                    'bookings.passcode as passcode',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('bookings.id', $id)
                ->orderBy('tanggal', 'desc')
                ->first();
        
        return view('Booking/edit', ['data' => $data]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ActivityController extends Controller
{
    public function index()
    {
        $data = DB::table('activities')
                ->select(
                    'activities.id as id',
                    'activities.tanggal as tanggal',
                    'activities.jam as jam',
                    'activities.tanggal as tanggal',
                    'activities.ip as ip',
                    'activities.activity as activity',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'activities.employee_nik', '=', 'employees.nik')
                ->orderBy('tanggal', 'desc')
                ->get();
        
        return view('Activity/index', ['data' => $data]);
    }

    public function turnOnLamp(Request $request)
    {
        return response()->json(true);
    }
}

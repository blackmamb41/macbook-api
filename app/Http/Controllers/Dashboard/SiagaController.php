<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiagaController extends Controller
{
    public function home() {
    	$employee = Employee::all();
    	return view('Siaga.index', ['user' => $employee]);
    }
}

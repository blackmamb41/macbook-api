<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel as EmployeeUser;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use App\Models\Booking;

class HomeController extends Controller
{
    public function index()
    {
        $employee = Employee::all()->count();
        $tiket = Booking::all()->count();
        $pending = Booking::where('status', 'pending')->get()->count();
        $cancel = Booking::where('status', 'cancel')->get()->count();
        $date = date('Y-m-d');
        $today = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('bookings.tanggal', $date)
                ->get();

        return view('Dashboard/index', ['employee' => $employee, 'tiket' => $tiket, 'pending' => $pending, 'cancel' => $cancel, 'today' => $today]);
    }

    public function login()
    {
        return view('Core/login');
    }

    public function logging()
    {
        $response = EmployeeUser::authenticate($creds);
        dd($response);
    }
}

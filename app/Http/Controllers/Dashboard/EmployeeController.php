<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    public function tableEmployee()
    {
        $data = Employee::orderBy('created_at', 'DESC')->get();

        return view('Employee/index', ['data' => $data]);
    }

    public function new()
    {
        return view('Employee/create');
    }

    public function create(Request $request)
    {
        if($request->photo == null)
        {
            $photo = '/user.png';
        } else {
            $photo = $request->photo;
        }
        Employee::create([
            'nik'       => $request->nik,
            'card_id'   => $request->card_id,
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'division'  => $request->division,
            'position'  => $request->position,
            'telp'      => $request->divison,
            'photo'     => $photo,
            'isAdmin'   => $request->isAdmin,
            'status'    => 1,
            'token'     => null,
            'expired'   => null,
            'remember_token' => null
        ]);

        return redirect('/employee');
    }

    public function edit($nik)
    {
        $data = Employee::where('nik', $nik)->first();
        // dd($data);

        return view('Employee/edit', ['data' => $data]);
    }

    public function status($nik)
    {
        $data = Employee::where('nik', $nik)->first();

        if($data->status == 0)
        {
            Employee::where('nik', $nik)->update(['status' => 1]);
        } else {
            Employee::where('nik', $nik)->update(['status' => 0]);
        }
        return redirect('/employee');
    }

    public function store(Request $request, $nik)
    {
        $data = Employee::where('nik', $nik)->first();
        Employee::where('nik', $nik)->update(array_filter($request->except('_token')));
        if($request->password == null) {
            return redirect('/employee');
        } else {
            Employee::where('nik', $nik)->update(['password' => Hash::make($request->password)]);
            return redirect('/employee');
        }
    }

    public function destroy($nik)
    {
        Employee::where('nik', $nik)->delete();

        return redirect('/employee');
    }
}

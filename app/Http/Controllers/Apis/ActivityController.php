<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\Employee;
use App\Models\Booking;

class ActivityController extends Controller
{
    public function appActivity(Request $request, $nik)
    {
        // $findEmployee = Employee::where('nik', $nik)->first();
        Activity::create([
            'activity'  => $request->activity,
            'tanggal'   => $request->tanggal,
            'jam'       => $request->time,
            'ip'        => $request->ip,
            'employee_nik' => $nik,
        ]);

        return response()->json(['message' => 'Activity Created']);
    }

    public function getIP(Request $request)
    {
        $ip = $request->ip();

        return response()->json(['ip' => $ip]);
    }

    public function turnOnLamp(Request $request)
    {
        return response()->json(['status' => 1]);
    }

    public function checkIn(Request $request, $card)
    {
        $date = date('Y-m-d');
        $pegawai = Employee::where('card_id', $card)->first();
        $token = $request->query('token');

        if($token == null || $pegawai == null){
            return response()->json(false);
        }elseif($token != $pegawai->token){
            return response()->json(false);
        }else{
            if(empty($pegawai)){
                return response()->json(false);
            }else {
                $checkInBooking = Booking::where([
                    ['employees_nik', $pegawai->nik],
                    ['tanggal', $date]
                ])->get();
                if($checkInBooking->isEmpty()){
                    return response()->json(false);
                }else{
                    foreach($checkInBooking as $data)
                    {
                        if($data->status == 'pending')
                        {
                            $update = Booking::find($data['id']);
                            $update->status = 'checkin';
                            $update->save();
                        }
                    }
                    return response()->json(true);
                }
            }
        }
    }

    public function gettingToken(Request $request){
        $id = $request->query('id');
        $getToken = Employee::where('card_id', $id)->first();

        return response()->json($getToken->token);
    }

    public function passcodeCheckin(Request $request){
        $passcode = $request->query('passcode');
        $getPasscodeInfo = Booking::where('passcode', $passcode)->first();
        $date = date('Y-m-d');
        $pegawai = Employee::where('card_id', $card)->first();
        $token = $request->query('token');

        if($token == null){
            return response()->json(false);
        }elseif($token != $pegawai->token){
            return response()->json(false);
        }else{
            if(empty($pegawai)){
                return response()->json(false);
            }else {
                $checkInBooking = Booking::where([
                    ['employees_nik', $pegawai->nik],
                    ['tanggal', $date]
                ])->get();
                if($checkInBooking->isEmpty()){
                    return response()->json(false);
                }else{
                    foreach($checkInBooking as $data)
                    {
                        if($data->status == 'pending')
                        {
                            $update = Booking::find($data['id']);
                            $update->status = 'checkin';
                            $update->save();
                        }
                    }
                    return response()->json(true);
                }
            }
        }
    }
}

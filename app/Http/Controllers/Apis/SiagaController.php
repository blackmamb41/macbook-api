<?php

namespace App\Http\Controllers\Apis;

use DB;
use Auth;;
use App\Models\Siaga;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiagaController extends Controller
{
    public function createSchedule(Request $request)
    {
    	$tanggal = $request->query('tanggal');
    	$shift = $request->query('s');
    	$user_list = serialize($request->users);
    	$findDuplicateDate = Siaga::where([
    							['tanggal', '=', $tanggal],
    							['shift', '=', $shift]
    						])->first();
    	if($findDuplicateDate != null){
    		return response()->json(['msg' => 'Already Exist'], 409);
    	}else{
    		Siaga::create([
    			'tanggal' 	=> $tanggal,
    			'shift'		=> $shift,
    			'users_list'	=> $user_list,
    			'id_user_created' => 4,
    			'status'	=> 1,
    		]);
    		return response()->json(['msg' => 'Success'], 200);
    	}
    }

    public function schedule(Request $request)
    {
        $shift = $request->query('s');
        $getSchedule = Siaga::where('shift', $shift)->orderBy('tanggal', 'ASC')->get();
        $arr = [];
        foreach ($getSchedule as $datas) {
            $users = unserialize($datas->users_list);
            $datas['users_list'] = $users;
            $datas['selected'] = Employee::select('id', 'name')->whereIn('id', $users)->get();
            $datas['notlisted'] = Employee::select('id', 'name')->whereNotIn('id', $users)->get();
        }
        unset($datas);

        return response()->json($getSchedule);
    }

    public function editSchedule(Request $request)
    {
        $id = $request->query('id');
        $tanggal = $request->tanggal;
        $shift = $request->shift;
        $user_list = serialize($request->users);
        $schedule = Siaga::find($id);
        $schedule->tanggal = $tanggal;
        $schedule->shift = $shift;
        $schedule->users_list = serialize($request->users);
        $schedule->save();

        return response()->json(['msg' => 'success']);
    }

    public function removeSchedule(Request $request)
    {
        $id = $request->idSchedule;
        Siaga::destroy($id);

        return response()->json(['msg' => 'deleted']);
    }

    public function mSchedule(Request $request)
    {
        $today = date('Y-m-d');
        $shift = $request->query('shift');
        $getSchedule = Siaga::where([
            ['tanggal', $today],
            ['shift', $shift]
        ])->get();
        $employeeSiaga = [];
        foreach ($getSchedule as $data) {
            $users = unserialize($data->users_list);
            $employeeSiaga = Employee::select('id', 'name')->whereIn('id', $users)->get();
        }
        unset($data);

        return response()->json($employeeSiaga);

    }

    public function changeStatusSiaga(Request $request)
    {
        $status = $request->query('status');
        $siaga = DB::table('settings')->where('key', 'siaga')->update(['value' => $status]);

        return response()->json(['stat' => $status]);
    }

    public function statusSiaga()
    {
        $getStatus = DB::table('settings')->where('key', 'siaga')->first();

        return response()->json($getStatus);
    }
}

<?php

namespace App\Http\Controllers\Apis;

use QrCode;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    public function addBooking(Request $request)
    {
        $getData = DB::table('employees')->get();
        $getLastTiket = DB::table('bookings')->orderBy('id', 'desc')->first();
        $getTiket = $getLastTiket->tiket;
        $getLastUniqeNumber = $getLastTiket->id+1;
        $perihal = $request->perihal;
        $tanggal = $request->tanggal;
        $tanggalBook = date('Y-m-d', strtotime($tanggal));
        $tiketTanggal = date('Ymd', strtotime($tanggal));
        $tiket = 'WS/' . $tiketTanggal . '/' . $getLastUniqeNumber;
        $jam = serialize($request->jam);
        $status = $request->status;
        $employees_nik = $request->employees_nik;
        $generate_code = (mt_rand(100000,999999));
        $findExistCode = Booking::where('passcode', $generate_code)->first();
        if($findExistCode == null){
            $passcode = mt_rand(100000,999999);
        }else{
            $passcode = $generate_code;
        }

        // QrCode::format('png')->size(500)->margin(0)->merge('/public/logo/telkomsel.png')->generate($passcode, public_path('qr/' . $passcode . '.png'));

        $addBook = Booking::create([
            'tiket'     => $tiket,
            'perihal'   => $perihal,
            'tanggal'   => $tanggalBook,
            'jam'       => $jam,
            'status'    => 'pending',
            'employees_nik' => $employees_nik,
            'passcode'  => $passcode,
            // 'qr'        => $passcode . '.png',
        ]);

        return response()->json($addBook);
    }

    public function checkBookingAndNIK($tanggal, $nik)
    {
        $date = strtotime($tanggal);
        $dateParams = date('Y-m-d', $date);
        $dataBooking = Booking::where([
            'tanggal'   => $dateParams,
            'employees_nik' => $nik
        ])->get();
        return response()->json(['data' => $dataBooking]);
    }

    public function checkBooking($tanggal)
    {
        $date = strtotime($tanggal);
        $dateParams = date('Y-m-d', $date);
        $dataBookingByTanggal = Booking::where('tanggal', $dateParams)->get();

        return response()->json(['data' => $dataBookingByTanggal]);
    }

    public function getSingleBooking($jam, $tanggal, $nik)
    {
        // dd(strtotime('Jan 29 2019 '));
        $date = strtotime($tanggal);
        $trueDate = date('Y-m-d', $date);
        dd($trueDate);
        $getSingleBook = Booking::where([
            'jam'       => $jam,
            'tanggal'   => $trueDate,
            'employees_nik' => $nik
        ])->first();
        
        return response()->json(['data' => $getSingleBook]);
    }

    public function getSingleTiket($id)
    {
        $getData = Booking::where('id', $id)->first();

        return response()->json($getData);
    }

    public function cancelBookingTiket($id)
    {
        Booking::destroy($id);

        return response()->json(['message' => 'Tiket was Deleted']);
    }

    public function checkJadwal()
    {
        $date = date('Y-m-d');
        $besok = date('Y-m-d', strtotime($date . "+1 days"));
        $lusa = date('Y-m-d', strtotime($date . "+2 days"));
        // $today = Booking::where('tanggal', $date)->get();
        $today = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.tiket as tiket',
                    'bookings.perihal as perihal',
                    'bookings.tanggal as tanggal',
                    'bookings.jam as jam',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('tanggal', $date)
                ->get();
        $tomorrow = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.tiket as tiket',
                    'bookings.perihal as perihal',
                    'bookings.tanggal as tanggal',
                    'bookings.jam as jam',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('tanggal', $besok)
                ->get();
        $aftertomorrow = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.tiket as tiket',
                    'bookings.perihal as perihal',
                    'bookings.tanggal as tanggal',
                    'bookings.jam as jam',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'employees.name as name',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('tanggal', $lusa)
                ->get();

        return response()->json(['today' => $today, 'tomorrow' => $tomorrow, 'aftertomorrow' => $aftertomorrow]);
    }

    public function generatePasscode(Request $request)
    {
        $tiket = $request->nomor;
        $check = Booking::where('tiket', $tiket)->first();
        $passcode = mt_rand(1000, 9999);
        $number = Booking::wherePasscode($passcode)->exists();
        if($number == false || $check->passcode == null) {
            Booking::where('tiket', $tiket)->update(['passcode' => $passcode]);
        }else{
            $passcode;
        }
        $get = Booking::where('tiket', $tiket)->first();

        return response()->json($get);
    }
}

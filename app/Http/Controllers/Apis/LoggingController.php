<?php

namespace App\Http\Controllers\Apis;

use DB;
use DateTime;
use App\Models\Booking;
use App\Models\Logging;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoggingController extends Controller
{
    public function checkPasscode(Request $request)
    {
        $passcode = $request->query('code');
        date_default_timezone_set('Asia/Jakarta');
        $getHour = date('H');
        $infoBooking = DB::table('bookings')
                ->select(
                    'bookings.id as id',
                    'bookings.tiket as tiket',
                    'bookings.perihal as perihal',
                    'bookings.tanggal as tanggal',
                    'bookings.jam as jam',
                    'bookings.status as status',
                    'bookings.employees_nik as nik',
                    'employees.name as name',
                    'employees.division as division',
                    'employees.card_id as card_id'
                )
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('passcode', $passcode)
                ->first();


        if(empty($infoBooking)){
            return response()->json(['msg' => 'Passcode Not Valid'], 401);
        }else if($passcode == ''){
            return response()->json(['msg' => 'No Code'], 400);
        }else{
            $today = date('Y-m-d');
            $date = new DateTime($today);
            $dateCheckin = date('Y-m-d H:i:s');
            $dateBooking = $infoBooking->tanggal;
            if($today < $dateBooking){
                return response()->json(['msg' => 'You Cannot Checkin Today'], 401);
            }else if($today > $dateBooking){
                return response()->json(['msg' => 'Your Booking Has Been Expired'], 401);
            }else{
                $checkLog = Logging::where('id_booking', $infoBooking->id)->where('checkin', 'like', $today.'%')->first();
                if($checkLog != null){
                    return response()->json(['msg' => "You're already Checkin"], 400);
                }else{
                    $bookingInfo = [
                        'tiket'     => $infoBooking->tiket,
                        'jam'       => unserialize($infoBooking->jam),
                        'name'      => $infoBooking->name,
                        'division'  => $infoBooking->division,
                        'perihal'   => $infoBooking->perihal
                    ];
                    $sortJam = unserialize($infoBooking->jam);
                    $ascJam = asort($sortJam);
                    $getLastJam = end($sortJam);
                    $getFirstJam = reset($sortJam);
                    $saveClockExpired = $date->setTime($getLastJam, 00, 00);
                    Logging::create([
                        'checkin'   => $dateCheckin,
                        'expired'   => $saveClockExpired,
                        'id_booking' => $infoBooking->id,
                        'status'    => 1
                    ]);
                    return response()->json([
                        'msg'       => 'Welcome and Please Enjoy',
                        'data'      => $bookingInfo,
                        'expired'   => $saveClockExpired->format('H:i:s'),
                        'start'     => $getFirstJam,
                        'end'       => $getLastJam,
                    ]);
                }
            }
        }
    }

    public function listCheckin(Request $request)
    {
        $today = date('Y-m-d');
        $list = DB::table('loggings')
                ->select(
                    'loggings.id as id',
                    'loggings.checkin as checkin',
                    'loggings.expired as expired',
                    'bookings.id as id_booking',
                    'bookings.employees_nik as nik',
                    'bookings.jam as jam',
                    'bookings.perihal as perihal',
                    'bookings.employees_nik as nik',
                    'employees.name as name'
                )
                ->leftJoin('bookings', 'loggings.id_booking', '=', 'bookings.id')
                ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                ->where('checkin', 'like', $today.'%')
                ->get();
        
        return response()->json(['list' => $list]);
    }

    public function scanCode(Request $request)
    {
        $user = $request->query('nik');
        $today = date('Y-m-d');
        date_default_timezone_set('Asia/Jakarta');
        $currentJam = intval(date('H'));
        $getUser = Employee::where('nik', $user)->first();
        if($getUser == null) {
            return response()->json(['msg' => 'No User Found']);
        }elseif($getUser->isAdmin == 1){
            file_get_contents("http://10.3.107.130:3000/door/unlock");
            sleep(10);
            file_get_contents("http://10.3.107.130:3000/door/lock");
            return response()->json(['status' => 'admin', 'msg' => 'You are Admin']);
        }else{
            $q = DB::table('bookings')
                    ->select(
                        'bookings.id as id',
                        'bookings.tiket as tiket',
                        'bookings.perihal as perihal',
                        'bookings.tanggal as tanggal',
                        'bookings.jam as jam',
                        'bookings.status as status',
                        'bookings.employees_nik as nik',
                        'employees.name as name',
                        'employees.division as division',
                        'employees.card_id as card_id'
                    )
                    ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                    ->where('nik', '=', $user)
                    ->where('tanggal', '=', $today)
                    ->get();

            $jam = [];
            $id = null;
            $count = 0;
            foreach ($q as $data) {
                $jams = unserialize($data->jam);
                foreach ($jams as $clock) {
                    if($clock === $currentJam) {
                        $id = $data->id;
                    }
                    $jam[$count] = $clock;
                    $count++;
                }
            }

            if(empty($id)) {
                $data = "No Booking Found";
                return response()->json(['data' => $data], 404);
            }else{
                $today = date('Y-m-d');
                $checkCheckin = Logging::where('id_booking', $id)->where('checkin', 'like', $today.'%')->first();
                if(!empty($checkCheckin)){
                    return response()->json(['status' => false, 'msg' => 'You Already Checkin']);
                }else{
                    $data = DB::table('bookings')
                        ->select(
                            'bookings.id as id',
                            'bookings.tiket as tiket',
                            'bookings.perihal as perihal',
                            'bookings.tanggal as tanggal',
                            'bookings.jam as jam',
                            'bookings.status as status',
                            'bookings.employees_nik as nik',
                            'employees.name as name',
                            'employees.division as division',
                            'employees.card_id as card_id'
                        )
                        ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                        ->where('bookings.id', $id)
                        ->first();

                    $today = date('Y-m-d');
                    $date = new DateTime($today);
                    $dateCheckin = date('Y-m-d H:i:s');
                    $dateBooking = $data->tanggal;
                    $bookingInfo = [
                        'tiket'     => $data->tiket,
                        'jam'       => unserialize($data->jam),
                        'name'      => $data->name,
                        'division'  => $data->division,
                        'perihal'   => $data->perihal
                    ];
                    $sortJam = unserialize($data->jam);
                    $ascJam = asort($sortJam);
                    $getLastJam = end($sortJam);
                    $getFirstJam = reset($sortJam);
                    $saveClockExpired = $date->setTime($getLastJam, 00, 00);
                    Logging::create([
                        'checkin'   => $dateCheckin,
                        'expired'   => $saveClockExpired,
                        'id_booking' => $data->id,
                        'status'    => 1
                    ]);
                    file_get_contents("http://10.3.107.130:3000/door/unlock");
                    return response()->json([
                        "status"    => true,
                        'msg'       => 'Welcome and Please Enjoy',
                        'data'      => $bookingInfo,
                        'expired'   => $saveClockExpired->format('H:i:s'),
                        'start'     => $getFirstJam,
                        'end'       => $getLastJam,
                    ]);
                }
            }
        }
    }

    public function checkStatusLastCheckin(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $user = $request->query('nik');
        $today = date('Y-m-d');
        $checkLog = Logging::where([
            ['checkin', 'like', $today.'%'],
            ['status', '=', 1]
        ])->orderBy('checkin', 'desc')->first();
        
        if($checkLog == null){
            return response()->json(['found' => false]);
        }else{
            $data = DB::table('bookings')
                    ->select(
                        'bookings.id as id',
                        'bookings.tiket as tiket',
                        'bookings.perihal as perihal',
                        'bookings.tanggal as tanggal',
                        'bookings.jam as jam',
                        'bookings.status as status',
                        'bookings.employees_nik as nik',
                        'employees.name as name',
                        'employees.division as division',
                        'employees.card_id as card_id'
                    )
                    ->leftJoin('employees', 'bookings.employees_nik', '=', 'employees.nik')
                    ->where('bookings.id', $checkLog->id_booking)
                    ->first();

                $today = date('Y-m-d');
                $date = new DateTime($today);
                $dateCheckin = date('Y-m-d H:i:s');
                $dateBooking = $data->tanggal;
                $bookingInfo = [
                    'tiket'     => $data->tiket,
                    'jam'       => unserialize($data->jam),
                    'name'      => $data->name,
                    'division'  => $data->division,
                    'perihal'   => $data->perihal
                ];
                $sortJam = unserialize($data->jam);
                $ascJam = asort($sortJam);
                $getLastJam = end($sortJam);
                $getFirstJam = reset($sortJam);
                $saveClockExpired = $date->setTime($getLastJam, 00, 00);
                return response()->json([
                    "found"    => true,
                    'msg'       => 'Welcome and Please Enjoy',
                    'data'      => $bookingInfo,
                    'expired'   => $saveClockExpired->format('H:i:s'),
                    'start'     => $getFirstJam,
                    'end'       => $getLastJam,
                ]);
        }
    }

    public function expiredTimeChangeStatus(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $user = $request->query('nik');
        $today = date('Y-m-d');
        $checkLog = Logging::where([
            ['checkin', 'like', $today.'%'],
            ['status', '=', 1]
        ])->orderBy('checkin', 'desc')->first();

        if($checkLog == null){
            return response()->json(['found' => false]);
        }else{
            Logging::where([
                ['checkin', 'like', $today.'%'],
                ['status', '=', 1]
            ])->orderBy('checkin', 'desc')->update(['status' => 0]);
            return response()->json(['found' => true, 'change' => 'success']);
        }
    }
}

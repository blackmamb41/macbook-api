<?php

namespace App\Http\Controllers\Apis;

use Assert\Assertion;
use Jose\Checker\ClaimCheckerInterface;
use Jose\Object\JWTInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jose\Factory\JWSFactory;
use Jose\Loader;
use Jose\JWTLoader;

class checkerController extends Controller
{
    public function checkToken(Request $request)
    {
        $token = $request->token;
        $jws = new JWTLoader($token);
        $getName = $jws->getPayload();

        // $getName = $token->getClaim('aud');
        return response()->json($jws);
    }
}

<?php

namespace App\Http\Controllers\Apis;

use App\Models\LMT;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LMTController extends Controller
{
    public function getActivePost()
    {
    	$all = LMT::orderBy('updated_at', 'desc')->where('status', 1)->get();

    	return response()->json($all);
    }

    public function showVideo($id)
    {
    	$video = LMT::where('id', $id)->first();

    	return response()->json($video->video);
    }
}

<?php

namespace App\Http\Controllers\Apis;

use App\Models\Booking;
use App\Models\Employee;
use Jose\Factory\JWKFactory;
use Jose\Factory\JWSFactory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    public function Login(Request $request)
    {
        $nik = $request->nik;
        $password = $request->password;
        // Logic Here
        if(empty($nik) && empty($password)) {
            return response()->json(['message' => 'username or password wrong'], 401);
        } elseif(empty($nik) || empty($password)) {
            return response()->json(['message' => 'username or password wrong'], 401);
        } else {
            $findUser = Employee::where('nik', $nik)->first();
            $check = Hash::check($password, $findUser->password);
            if($check == false) {
                return response()->json(['message' => 'username or password wrong'], 401);
            } else {
                // Generate Token
                $token = Hash::make($password);
                $dateNow = date_create(date('Y-m-d'));
                date_add($dateNow,date_interval_create_from_date_string("300 days"));
                $date = date_format($dateNow, 'Y-m-d');
                Employee::where('nik', $nik)->update(['token' => $token, 'expired' => $date]);
                return response()->json($token);
            }
        }
    }

    public function allEmployee(Request $request)
    {
        $all = Employee::all();
        $q = $request->query('filter');
        if($q != null) {
            $forFilter = Employee::select('id', 'name')->get();
            return response($forFilter);
        }else {
            return response()->json(['data' => $all]);
        }
    }

    public function getSingleEmployee($nik)
    {
        $dataEmployee = Employee::where('nik', $nik)->first();

        return response()->json($dataEmployee);
    }

    public function getHistory($nik)
    {
        $getAllHistoryByNik = Booking::where('employees_nik', $nik)->orderBy('id', 'desc')->get();

        return response()->json($getAllHistoryByNik);
    }

}
